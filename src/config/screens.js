import { Navigation } from 'react-native-navigation';

import LoginScreen from '../components/authentication/moduls/login/loginScreen';
import SignupScreen from '../components/authentication/moduls/signup/signupScreen';
import SignupPasswordScreen from '../components/authentication/moduls/signup/signupScreenPassword';
import SignupSuccessScreen from '../components/authentication/moduls/signup/signupScreenSuccess';
import ForgotPasswordScreen from '../components/authentication/moduls/forgotPassword/forgotPasswordScreen';
import MainMapScreen from '../components/home/moduls/mainMap';
import LaporIntroScreen from '../components/home/moduls/report/main';
import LaporSuccessScreen from '../components/home/moduls/report/reportSuccess';

import lightboxAlert from '../components/modul/lightboxAlert';
import lightboxConfirm from '../components/modul/lightboxConfirm';
import drawerScreen from '../components/modul/drawer';
import ProfileScreen from '../components/drawer/profile';
import SettingScreen from '../components/drawer/setting';
import signupScreenPassword from '../components/authentication/moduls/signup/signupScreenPassword';
import signupScreenSuccess from '../components/authentication/moduls/signup/signupScreenSuccess';

export function registerScreens(store, Provider) {
    Navigation.registerComponent('damkar.auth.login', () => LoginScreen, store, Provider);
    Navigation.registerComponent('damkar.auth.signup', () => SignupScreen, store, Provider);
    Navigation.registerComponent('damkar.auth.signupPassword', () => signupScreenPassword, store, Provider);
    Navigation.registerComponent('damkar.auth.signupSuccess', () => signupScreenSuccess, store, Provider);
    Navigation.registerComponent('damkar.auth.forgotPassword', () => ForgotPasswordScreen, store, Provider);
    Navigation.registerComponent('damkar.home.map', () => MainMapScreen, store, Provider);
    Navigation.registerComponent('damkar.home.lapor.intro', () => LaporIntroScreen, store, Provider);
    Navigation.registerComponent('damkar.home.lapor.success', () => LaporSuccessScreen, store, Provider);
    Navigation.registerComponent('damkar.lightbox.alert', () => lightboxAlert, store, Provider);
    Navigation.registerComponent('damkar.lightbox.confirm', () => lightboxConfirm, store, Provider);
    Navigation.registerComponent('damkar.drawer.main', () => drawerScreen, store, Provider);
    Navigation.registerComponent('damkar.drawer.profile', () => ProfileScreen, store, Provider);
    Navigation.registerComponent('damkar.drawer.setting', () => SettingScreen, store, Provider);
}
  
export function registerScreensVisibilityListener(){
  new ScreensVisibilityListener({
    
  }).register();
}