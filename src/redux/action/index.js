import * as types from './actiontypes';

/*
Action Creators
*/

export function changeAppRoot(root) {
  return {
    type: types.ROOT_CHANGED,
    root: root
  };
}

/*
dispatch the actionCreators 
*/

export function appInitialized() {
  return async function(dispatch, getState) {
    dispatch(changeAppRoot('login'));
  };
}

export function login() {
  return async function(dispatch, getState) {
    dispatch(changeAppRoot('after-login'));
  };
}