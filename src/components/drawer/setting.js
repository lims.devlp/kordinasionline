import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
    View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, Keyboard
} from "react-native";
import ElevatedView from 'react-native-elevated-view';
import ImagePicker from 'react-native-image-picker';
import VIcon from '../../components/styles/VIcon';
import { connect } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import * as appActions from '../../redux/action';

const screen = Dimensions.get('window');

export class setting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSave: false,
            isEditAddress: false,
            isEditEmail: false,
            isEditPhone: false,
            img_uri: null,
        }
    }

    editProfile = (val) => {
        this.setState({ isSave: true });
        if (val == 'address') {
            this.setState({ isEditAddress: true });
            this._address.focus();
        } else if (val == 'email') {
            this.setState({ isEditEmail: true });
            this._email.focus();
        } else if (val == 'phone') {
            this.setState({ isEditPhone: true });
            this._phone.focus();
        }
    }

    goBack = () => {
        this.props.navigator.pop();
    }

    saveProfile = () => {
        Keyboard.dismiss();
        this.setState({
            isSave: false,
            isEditAddress: false,
            isEditEmail: false,
            isEditPhone: false
        })
        this.props.navigator.showSnackbar({
            text: 'Password berhasil diubah.',
            // actionText: 'done', // optional
            // actionId: 'fabClicked', // Mandatory if you've set actionText
            // actionColor: 'green', // optional
            textColor: 'black', // optional
            backgroundColor: '#e5c059', // optional
            duration: 'short' // default is `short`. Available options: short, long, indefinite
        });
    }

    openCamera = () => {
        this.setState({ isSave: true });
        var options = {
            title: 'Select Picture',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.launchCamera(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else {
                let source = response.uri;
                let path = response.path;
                this.setState({
                    img_uri: 'file://' + path
                });
            }
        });
        console.log("image uri = " + this.state.img_uri);
    }

    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#5459E6', '#FF3737']} style={Styles.linearGradient}>
                    <View style={{ flexDirection: 'row', marginTop: 15, justifyContent: 'space-between', alignItems: 'center' }}>
                        <TouchableOpacity onPress={this.goBack}>
                            <VIcon type={VIcon.TYPE_FEATHER} name="arrow-left" size={25} color='white' />
                        </TouchableOpacity>
                        {
                            this.state.isSave ?
                                <TouchableOpacity onPress={this.saveProfile}>
                                    <Text style={{ fontSize: screen.height * 0.03, color: 'white' }}>Simpan</Text>
                                </TouchableOpacity> :
                                null
                        }
                    </View>
                    <View style={{ justifyContent: 'center', marginTop: 20 }}>
                        <Text style={{ fontSize: screen.height * 0.05, color: 'white' }}>Ubah</Text>
                        <Text style={{ fontSize: screen.height * 0.05, color: 'white' }}>Password</Text>
                    </View>
                </LinearGradient>
                <ScrollView keyboardShouldPersistTaps='always'>
                    <Text style={Styles.wrapperLabel}>Password lama</Text>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 5 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <View style={Styles.wrapperTextInput}>
                                <TextInput
                                    style={{ width: screen.width * 0.68 }}
                                    ref={oldpass => this._oldpass = oldpass}
                                    marginLeft={10}
                                    placeholder={'Your Old Password'}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                />
                                <TouchableOpacity style={{ marginRight: 10 }}>
                                    <VIcon type={VIcon.TYPE_ENTYPO} name="eye" size={20} color='#808080' />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <Text style={Styles.wrapperLabel}>Password baru</Text>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 5 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <View style={Styles.wrapperTextInput}>
                                <TextInput
                                    style={{ width: screen.width * 0.68 }}
                                    ref={newpass => this._newpass = newpass}
                                    marginLeft={10}
                                    placeholder={'Your New Password'}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                />
                                <TouchableOpacity style={{ marginRight: 10 }}>
                                    <VIcon type={VIcon.TYPE_ENTYPO} name="eye" size={20} color='#808080' />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <Text style={Styles.wrapperLabel}>Konfirmasi password </Text>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 5 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <View style={Styles.wrapperTextInput}>
                                <TextInput
                                    style={{ width: screen.width * 0.68 }}
                                    ref={conpass => this._conpass = conpass}
                                    marginLeft={10}
                                    placeholder={'Confirm New Password'}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                />
                                <TouchableOpacity style={{ marginRight: 10 }}>
                                    <VIcon type={VIcon.TYPE_ENTYPO} name="eye" size={20} color='#808080' />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    wrapper: {
        width: screen.width * 0.75,
        height: screen.height,
        overflow: 'hidden',
    },
    linearGradient: {
        width: screen.width,
        height: screen.height * 0.3,
        paddingLeft: 10,
        paddingRight: 10,
    },
    wrapperLabel: {
        fontWeight: 'bold',
        color: 'black',
        marginTop: screen.height * 0.05,
        marginLeft: screen.width * 0.1
    },
    wrapperTextInput: {
        width: screen.width * 0.8,
        height: screen.height * 0.07,
        borderColor: '#B4B4B4',
        borderRadius: 10,
        borderWidth: 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
});

function mapStateToProps(state, ownProps) {
    return {
        rootState: state.root
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(appActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(setting);