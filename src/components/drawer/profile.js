import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
    View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, Keyboard
} from "react-native";
import ElevatedView from 'react-native-elevated-view';
import ImagePicker from 'react-native-image-picker';
import VIcon from '../../components/styles/VIcon';
import { connect } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import * as appActions from '../../redux/action';

const screen = Dimensions.get('window');

export class profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSave: false,
            isEditAddress: false,
            isEditEmail: false,
            isEditPhone: false,
            img_uri: null,
        }
    }

    editProfile = (val) => {
        this.setState({ isSave: true });
        if (val == 'address') {
            this.setState({ isEditAddress: true });
            this._address.focus();
        } else if (val == 'email') {
            this.setState({ isEditEmail: true });
            this._email.focus();
        } else if (val == 'phone') {
            this.setState({ isEditPhone: true });
            this._phone.focus();
        }
    }

    goBack = () => {
        this.props.navigator.pop();
    }

    saveProfile = () => {
        Keyboard.dismiss();
        this.setState({
            isSave: false,
            isEditAddress: false,
            isEditEmail: false,
            isEditPhone: false
        })
        this.props.navigator.showSnackbar({
            text: 'Data berhasil disimpan.',
            // actionText: 'done', // optional
            // actionId: 'fabClicked', // Mandatory if you've set actionText
            // actionColor: 'green', // optional
            textColor: 'black', // optional
            backgroundColor: '#e5c059', // optional
            duration: 'short' // default is `short`. Available options: short, long, indefinite
        });
    }

    openCamera = () => {
        this.setState({ isSave: true });
        var options = {
            title: 'Select Picture',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else {
                let source = response.uri;
                let path = response.path;
                this.setState({
                    img_uri: 'file://' + path
                });
            }
        });
        console.log("image uri = " + this.state.img_uri);
    }

    render() {
        return (
            <View style={{ backgroundColor: '#F5F5F5', flex: 1 }}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#5459E6', '#FF3737']} style={Styles.linearGradient}>
                    <View style={{ flexDirection: 'row', marginTop: 15, justifyContent: 'space-between', alignItems: 'center' }}>
                        <TouchableOpacity onPress={this.goBack}>
                            <VIcon type={VIcon.TYPE_FEATHER} name="arrow-left" size={25} color='white' />
                        </TouchableOpacity>
                        {
                            this.state.isSave ?
                                <TouchableOpacity onPress={this.saveProfile}>
                                    <Text style={{ fontSize: 16, color: 'white' }}>Simpan</Text>
                                </TouchableOpacity> :
                                null
                        }
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', marginTop: 10, flexDirection: 'row' }}>
                        <View style={{ height: screen.height * 0.13, width: screen.height * 0.13, borderRadius: screen.height * 0.13 * 0.5, borderColor: 'white', borderWidth: 3, justifyContent: 'center', alignItems: 'center' }}>
                            {
                                this.state.img_uri != null ?
                                    <Image style={{ height: screen.height * 0.13, width: screen.height * 0.13, borderRadius: screen.height * 0.13 * 0.5 }} source={{ uri: this.state.img_uri }} /> :
                                    <Text style={{ fontSize: screen.height * 0.05, color: 'white' }}>KR</Text>
                            }
                        </View>
                        <ElevatedView elevation={3} style={{
                            width: screen.height * 0.05, height: screen.height * 0.05,
                            backgroundColor: '#EC008C',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: screen.height * 0.05 * 0.5,
                            marginLeft: -20
                        }}>
                            <TouchableOpacity onPress={this.openCamera}>
                                <VIcon type={VIcon.TYPE_FEATHER} name="camera" size={18} color="white" />
                            </TouchableOpacity>
                        </ElevatedView>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                        <Text style={{ fontSize: 16, color: 'white' }}>Kripton Haz</Text>
                    </View>
                </LinearGradient>
                <ScrollView keyboardShouldPersistTaps='always'>
                <View style={Styles.wrapperLabel}>
                        <Text style={Styles.wrapperLabelText}>Address</Text>
                        <TouchableOpacity onPress={() => this.editProfile('address')}>
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="pencil" size={25} color='#808080' style={{ marginRight: 10 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={Styles.wrapperLabelInput}>
                        <TextInput
                            ref={address => this._address = address}
                            editable={this.state.isEditAddress}
                            marginLeft={10}
                            placeholder={'Alamat anda'}
                            multiline={true}
                            underlineColorAndroid='rgba(0,0,0,0)'
                        />
                    </View>
                    <View style={Styles.wrapperLabel}>
                        <Text style={Styles.wrapperLabelText}>Email</Text>
                        <TouchableOpacity onPress={() => this.editProfile('email')}>
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="pencil" size={25} color='#808080' style={{ marginRight: 10 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={Styles.wrapperLabelInput}>
                        <TextInput
                            ref={email => this._email = email}
                            editable={this.state.isEditEmail}
                            marginLeft={10}
                            placeholder={'Email anda'}
                            underlineColorAndroid='rgba(0,0,0,0)'
                        />
                    </View>
                    <View style={Styles.wrapperLabel}>
                        <Text style={Styles.wrapperLabelText}>Phone</Text>
                        <TouchableOpacity onPress={() => this.editProfile('phone')}>
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="pencil" size={25} color='#808080' style={{ marginRight: 10 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={Styles.wrapperLabelInput}>
                        <TextInput
                            ref={phone => this._phone = phone}
                            editable={this.state.isEditPhone}
                            marginLeft={10}
                            placeholder={'Nomer telepon anda'}
                            underlineColorAndroid='rgba(0,0,0,0)'
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    wrapper: {
        width: screen.width * 0.75,
        height: screen.height,
        overflow: 'hidden',
    },
    linearGradient: {
        width: screen.width,
        height: screen.height * 0.3,
        paddingLeft: 10,
        paddingRight: 10,
    },
    wrapperLabel: {
        backgroundColor: 'white',
        justifyContent: 'space-between',
        marginTop: 10,
        height: screen.height * 0.07,
        flexDirection: 'row',
        alignItems: 'center'
    },
    wrapperLabelText: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 14,
        marginLeft: 10
    },
    wrapperLabelInput: {
        backgroundColor: 'white',
        justifyContent: 'center',
        marginTop: 2
    }
});

function mapStateToProps(state, ownProps) {
    return {
        rootState: state.root
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(appActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(profile);