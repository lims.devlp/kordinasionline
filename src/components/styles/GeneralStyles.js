import { StyleSheet, Dimensions } from 'react-native';
var screen = Dimensions.get('window');

export default StyleSheet.create({
    imageBackground:{
        width: '100%', 
        height: '100%'
    },
});