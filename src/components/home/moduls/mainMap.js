import React, { Component } from 'react';
import {
    AppRegistry, View, StatusBar, Button, Text, StyleSheet,
    Dimensions, AsyncStorage, TouchableOpacity, Animated, Easing, ImageBackground,
    PermissionsAndroid, TouchableWithoutFeedback, Image, Linking
} from 'react-native';
import MapView, { Marker, Polyline } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import Carousel from 'react-native-snap-carousel';
import ElevatedView from 'react-native-elevated-view';
import getDirections from 'react-native-google-maps-directions';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import firebase from 'react-native-firebase';
import type { Notification, NotificationOpen } from 'react-native-firebase';
import type { RemoteMessage } from 'react-native-firebase';
import NetworkState, { Settings } from 'react-native-network-state';
import RNExitApp from 'react-native-exit-app';
import { connect } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import * as appActions from '../../../redux/action';
import VIcon from '../../../components/styles/VIcon';
var screen = Dimensions.get('window');

export class mainMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mapRegion: {
                latitude: 0.0,
                longitude: 0.0,
                latitudeDelta: 0.0222,
                longitudeDelta: 0.0222,
            },
            cur_lat: null,
            cur_long: null,
            lastLat: null,
            lastLong: null,
            defaultData: [
                {
                    cabang: "Pos Mampang Prapatan",
                    alamat: "Jl. Kapten Tendean No.34 RT. 01/02 Mampang Prapatan\nJakarta 12710",
                    location: {
                        latitude: -6.2400144,
                        longitude: 106.8267256
                    },
                    duration: "0 mins",
                    onPress: () => this.makePhonecall('02122792395'),
                    onDirection: (loc) => this.getDirection(loc),
                    photo: require('../../../assets/office_icon.png'),
                },
                {
                    cabang: "Pos Kemang Village",
                    alamat: "Jl. Kemang No.1B RT. 012/05 Mampang Prapatan\nJakarta 12730",
                    location: {
                        latitude: -6.259090,
                        longitude: 106.812305
                    },
                    duration: "0 mins",
                    onPress: () => this.makePhonecall('085691989294'),
                    onDirection: (loc) => this.getDirection(loc),
                    photo: require('../../../assets/office_icon.png'),
                }
            ],
            error: '',
            error_msg: '',
        }
    }

    async componentDidMount() {
        this.fcmListener();
        try {
            const grantedLocation = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Location Permission',
                    'message': 'this apps needs access to your location.'
                }
            );
            if (grantedLocation === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the camera")
                Geolocation.getCurrentPosition(
                    (position) => {
                        console.log(position);
                        this.setState(({ mapRegion }) => ({
                            mapRegion: {
                                ...mapRegion,
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                            }
                        }));
                        this.setState({
                            cur_lat: position.coords.latitude,
                            cur_long: position.coords.longitude,
                        });
                    },
                    (error) => {
                        console.log(error.code, error.message);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
            } else {
                console.log("Camera permission denied")
            }

            const grantedCall = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CALL_PHONE,
                {
                    'title': 'Phone Call Permission',
                    'message': 'this apps needs access to your phone call.'
                }
            );
            console.log("grantedCall = " + grantedCall);
            const grantedLog = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
                {
                    'title': 'Read Call Log Permission',
                    'message': 'this apps needs access to your call log.'
                }
            );
            console.log("grantedLog = " + grantedLog);
            // const grantedNetwork = await PermissionsAndroid.request(
            //     PermissionsAndroid.PERMISSIONS.ACCESS_NETWORK_STATE,
            //     {
            //         'title': 'Access Network State Permission',
            //         'message': 'this apps needs access to your network state.'
            //     }
            // );
            // console.log("grantedNetwork = "+grantedNetwork);
        } catch (err) {
            console.warn(err)
        }
    }

    fcmListener = () => {
        const channel = new firebase.notifications.Android.Channel('damkar-online', 'Channel Damkar', firebase.notifications.Android.Importance.Max)
            .setDescription('damkar channel');
        firebase.notifications().android.createChannel(channel);
        firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    console.log("fcmToken = " + fcmToken);
                } else {
                    console.log("user doesn't have a token");
                }
            });
        this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
            const localNotification = new firebase.notifications.Notification({
                sound: 'default',
                show_in_foreground: true,
            })
                .setNotificationId(message.messageId)
                .setTitle(message.data.title)
                .setSubtitle(message.data.subtitle)
                .setBody(message.data.body)
                .setData(message.data)
                .android.setChannelId('damkar-online') // e.g. the id you chose above
                .android.setSmallIcon('logo_damkar') // create this icon in Android Studio
                .android.setColor('#ffffff') // you can set a color here
                .android.setPriority(firebase.notifications.Android.Priority.High)
                .android.setAutoCancel(true)
                .android.setVisibility(firebase.notifications.Android.Visibility.Public);
            firebase.notifications().displayNotification(localNotification);
        });
        firebase.notifications().onNotificationOpened(n => {
            console.log("Akhirnya dibuka: ", n);
        });
    }

    snapTo = (item, index) => {
        if (index - 3 > this._carousel.currentIndex) {
            this._carousel.snapToNext()
        } else if (index - 3 < this._carousel.currentIndex) {
            this._carousel.snapToPrev()
        }
    }

    renderCarousel = ({ item, index }) => {
        return (
            <TouchableWithoutFeedback onPress={() => this.snapTo(item, index)}>
                <ElevatedView elevation={3} 
                    onTouchStart={this.focusTo}
                    style={{
                    height: screen.height * 0.25,
                    backgroundColor: '#F5F5F5',
                    borderRadius: 10,
                    marginVertical: 12,
                    marginHorizontal: 10
                }}>
                    <View style={{flex: 0.6, borderTopLeftRadius: 10, borderTopRightRadius: 10}}>
                        <View style={{ backgroundColor: 'white', borderTopLeftRadius: 10, borderTopRightRadius: 10}}>
                            {/* <Image source={item.photo} style={{ borderTopLeftRadius: 12, borderTopRightRadius: 12, width: '100%', height: '100%', resizeMode: 'cover' }} /> */}
                            <View style={{ flexDirection: 'row', justifyContent: "center", alignItems: 'flex-start', marginTop: 10 }}>
                                <VIcon type={VIcon.TYPE_FONTAWESOME} name="map-marker" size={20} color='#FC2C73' />
                                <Text style={{ fontFamily: 'Segoe UI', fontSize: 14, color: 'black', marginLeft: 5, fontWeight: 'bold' }}>{item.cabang}</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomLeftRadius: 5, borderBottomRightRadius: 5, backgroundColor: '#fff', width: "100%", padding: 8 }}>
                            <Text style={{ fontSize: 12, fontFamily: 'calibri', marginTop: 5 }}>{item.alamat}</Text>
                        </View>
                    </View>
                    <View style={{flex: 0.4, justifyContent: 'center',alignItems: 'center'}}>
                        <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <TouchableOpacity onPress={()=>this.getDirection(item)} style={{backgroundColor: '#5459E6', width: '40%', height: '40%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 20, marginHorizontal: 5}}>
                                <VIcon type={VIcon.TYPE_MATERIALCOMMUNITYICONS} name="google-maps" size={20} color='white' />
                                <Text style={{ fontFamily: 'Segoe UI', fontSize: 14, color: 'white', marginLeft: 5, fontWeight: '500' }}>Detail</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.callFiretruck} style={{backgroundColor: '#FF3737', width: '40%', height: '40%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 20, marginHorizontal: 5}}>
                                <VIcon type={VIcon.TYPE_FOUNDATION} name="megaphone" size={20} color='white' />
                                <Text style={{ fontFamily: 'Segoe UI', fontSize: 14, color: 'white', marginLeft: 5, fontWeight: '500' }}>Lapor</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ElevatedView>
            </TouchableWithoutFeedback>
        );
    }

    focusTo = () => {
        this._map.animateToCoordinate(this.state.defaultData[this._carousel.currentIndex].location, 2000);
    }

    showCurrentLocation = () => {
        console.log("current location");    
        this._map.animateToCoordinate(this.state.mapRegion, 2000);
    }

    callFiretruck = () => {
        this.props.navigator.push({
            screen: 'damkar.home.lapor.intro',
            navigatorStyle: {
              navBarHidden: true,
            },
        });
    }

    openLightbox = (data, act) => {
        this.props.navigator.showLightBox({
            screen: 'damkar.lightbox.alert',
            style: {
                backgroundBlur: 'dark',
                backgroundColor: '#44444480',
                tapBackgroundToDismiss: false
            },
            passProps: {
                data: () => (data),                
            }
        })
    }

    makePhonecall = (number) => {
        console.log("make phonecall");
        const args = {
            number: number,
            prompt: true
        }
        RNImmediatePhoneCall.immediatePhoneCall(number);
    }

    getDirection = (loc) => {
        console.log("location = " + JSON.stringify(loc));
        const data = {
            source: {
                latitude: loc.location.latitude,
                longitude: loc.location.longitude
            },
            destination: {
                latitude: this.state.cur_lat,
                longitude: this.state.cur_long
            },
            params: [
                {
                    key: "travelmode",
                    value: "driving"        // may be "walking", "bicycling" or "transit" as well
                },
                {
                    key: "dir_action",
                    //value: "navigate"       // this instantly initializes navigation using the given travel mode 
                }
            ]
        }
        // getDirections(data)
        Linking.openURL('https://www.google.com/maps/search/?api=1&query='+loc.location.latitude+','+loc.location.longitude);
    }

    toggleDrawer = () => {
        console.log("toggle drawer");
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        });
    }

    actionDisconnect = () => {
        const data = {
            message: 'Pastikan anda terhubung ke jaringan',
            act: true,
            test: () => {this.exitApps()}
        }
        this.openLightbox(data);
    }

    exitApps = () => {
        RNExitApp.exitApp();
    }

    render() {
        return (
            <View style={{ height: screen.height, flex: 1 }}>
                <NetworkState 
                    txtConnected={"Anda kembali terhubung"}
                    txtDisconnected={"Anda terputus dari jaringan"}
                    onDisconnected={() => {this.actionDisconnect()}}
                />
                <MapView
                    style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, }}
                    showsUserLocation={true}
                    ref={m => this._map = m}
                    region={this.state.mapRegion}
                    showsMyLocationButton={false}
                    provider={this.props.provider}
                >
                    {
                        this.state.defaultData.map((item, index) => {
                            return <Marker key={index} title={item.cabang} coordinate={item.location} />
                        })
                    }
                </MapView>

                <View style={{ top: screen.height * 0.05, left: screen.width * 0.1, position: 'absolute' }}>
                    <ElevatedView elevation={3} style={{
                        borderRadius: 5,
                        width: 50, height: 50,
                        backgroundColor: 'white',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <TouchableOpacity
                            onPress={this.toggleDrawer}
                        >
                            <VIcon type={VIcon.TYPE_MATERIALCOMMUNITYICONS} name="menu" size={28} color="black" />
                        </TouchableOpacity>
                    </ElevatedView>
                </View>

                {/* <View style={{ top: screen.height * 0.43, left: screen.width * 0.85, position: 'absolute' }}>
                    <ElevatedView elevation={3} style={{
                        width: 50, height: 50, borderRadius: 25,
                        backgroundColor: 'white',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <TouchableOpacity
                            onPress={this.callFiretruck}
                        >
                            <VIcon type={VIcon.TYPE_MATERIALCOMMUNITYICONS} name="fire-truck" size={28} color="red" />
                        </TouchableOpacity>
                    </ElevatedView>
                </View> */}

                <View style={{ top: screen.height * 0.57, left: screen.width * 0.85, position: 'absolute' }}>
                    <ElevatedView elevation={3} style={{
                        width: 50, height: 50, borderRadius: 25,
                        backgroundColor: 'white',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <TouchableOpacity
                            onPress={this.showCurrentLocation}
                        >
                            <VIcon type={VIcon.TYPE_MATERIALICONS} name="my-location" size={28} color="#FC2C73" />
                        </TouchableOpacity>
                    </ElevatedView>
                </View>

                <View style={{ position: 'absolute', bottom: 0 }}>
                    <Carousel
                        onSnapToItem={this.focusTo}
                        ref={(c) => { this._carousel = c; }}
                        loop={true}
                        data={this.state.defaultData}
                        renderItem={this.renderCarousel.bind(this)}
                        sliderWidth={screen.width}
                        itemWidth={screen.width / 1.5}
                    />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        rootState: state.root
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(appActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(mainMap);