import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image, Dimensions, 
    Linking, AppState, TextInput } from 'react-native';
import { Row, Container, Content, Grid, Button, Form, Item, Icon, Input } from "native-base";
import { connect } from 'react-redux';
import * as appActions from '../../../../redux/action';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import { WINDOW } from '../../../constants';
import VIcon from '../../../styles/VIcon';
import LinearGradient from 'react-native-linear-gradient';
import StepIndicator from 'react-native-step-indicator';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import CallLogs from 'react-native-call-log';
import ImagePicker from 'react-native-image-picker';
import ElevatedView from 'react-native-elevated-view';

type Props = {};
export class LaporIntro extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
        appState: AppState.currentState,
        phoneNumber: '',
        isCall: false,
        isCamera: false,
        counterAppstate: 0,
        callDuration: -1,
        currentPosition: 0,
        colorPhone: '#5459E6',
        colorCamera: '#979afc',
        colorReport: '#979afc',
        colorLabelPhone: 'black',
        colorLabelCamera: '#D4D4D4',
        colorLabelReport: '#D4D4D4',
        img_uri: null,
    }
  }

  componentDidMount(){
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      console.log("counterAppState = "+this.state.counterAppstate);
      this.setState({counterAppstate: this.state.counterAppstate+1});
      if(this.state.counterAppstate > 1){
        this.setState({counterAppstate: 0});
        console.log("reset counterAppstate to "+this.state.counterAppstate);
        if(this.state.isCall == true){
          this.setState({isCall: false});
          this.getCallLog();
        }
      }
    }
    this.setState({appState: nextAppState});
  }

  openLightbox = (data) => {
    this.props.navigator.showLightBox({
        screen: 'damkar.lightbox.alert',
        style: {
            backgroundBlur: 'dark',
            backgroundColor: '#44444480',
            tapBackgroundToDismiss: true
        },
        passProps: {
            data: () => (data)
        }
    })
  }

  gotoFinish = () => {
    this.props.navigator.push({
      screen: 'damkar.home.lapor.success',
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }

  goBack = () => {
    this.props.navigator.pop();
  }

  stepIndicator = (param) =>{
    if(param == "next"){
        this.setState({
            currentPosition: this.state.currentPosition+1,
        });
    } else if(param == "back"){
        this.setState({currentPosition: this.state.currentPosition-1 });
    }
    console.log("current position = "+this.state.currentPosition);
    if(this.state.currentPosition == 0){
        console.log("active phone");
        this.setState({
            colorPhone: '#5459E6',
            colorCamera: '#979afc',
            colorReport: '#979afc',
            colorLabelPhone: 'black',
            colorLabelCamera: '#D4D4D4',
            colorLabelReport: '#D4D4D4'
        });
    } else if(this.state.currentPosition == 1){
        console.log("active camera");
        this.setState({
            colorPhone: '#979afc',
            colorCamera: '#5459E6',
            colorReport: '#979afc',
            colorLabelPhone: '#D4D4D4',
            colorLabelCamera: 'black',
            colorLabelReport: '#D4D4D4'
        });
    } else if(this.state.currentPosition == 2){
        console.log("active report");
        this.setState({
            colorPhone: '#979afc',
            colorCamera: '#979afc',
            colorReport: '#5459E6',
            colorLabelPhone: '#D4D4D4',
            colorLabelCamera: '#D4D4D4',
            colorLabelReport: 'black'
        });
    }
  }

    makePhonecall = (number) => {
        console.log("make phonecall");
        const args = {
            number: number,
            prompt: true
        }    
        RNImmediatePhoneCall.immediatePhoneCall(number);
        this.setState({isCall: true, phoneNumber: number});
        console.log("iscall = "+this.state.isCall);
    }

    getCallLog = () =>{
        CallLogs.show((logs) =>{
             const parsedLogs = JSON.parse(logs);
             console.log("hasil log phone = "+parsedLogs[0].phoneNumber);
             console.log("hasil log durasi = "+parsedLogs[0].callDuration);
             this.setState({callDuration: parsedLogs[0].callDuration});
             if(parsedLogs[0].phoneNumber != this.state.phoneNumber || parsedLogs[0].callDuration == 0){
                const data = {
                    message: "Silahkan menghubungi petugas kembali"
                }
                this.openLightbox(data);
            } else{
                this.stepIndicator("next");
            }
        });
    }

    openCamera = () => {
      this.setState({ isCamera: true });
      var options = {
          title: 'Select Picture',
          storageOptions: {
              skipBackup: true,
              path: 'images'
          }
      };
      ImagePicker.launchCamera(options, (response) => {
          console.log('Response = ', response);
          if (response.didCancel) {
              console.log('User cancelled image picker');
          }
          else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
          }
          else {
              let source = response.uri;
              let path = response.path;
              this.setState({
                  img_uri: 'file://' + path
              });
              this.stepIndicator("next");
              console.log("image uri = " + "file://"+ path);
          }
      });
    }

  render() {
    let error = false;
    if (this.props.form) {
      if (this.props.form.syncErrors) {
        error = true;
      } else {
        error = false;
      }
    } else {
      error = false;
    }
    return (
      <View style={{justifyContent: 'space-between', height: WINDOW.height}}>
        <View style={{flex: 0.92}}>
          <View style={{height: WINDOW.height*0.2, backgroundColor: '#5459E6'}}>
            <TouchableOpacity onPress={this.goBack} style={{marginLeft: 10, marginTop: 10}}>
                <VIcon type={VIcon.TYPE_FEATHER} name="arrow-left" size={25} color='white' />
            </TouchableOpacity>
            <View style={{ width: WINDOW.width, height: WINDOW.height*0.1, marginTop: 10, marginLeft: 10}}>
                <Text style={{fontFamily: 'Segoe UI', fontSize: WINDOW.height*0.07, color: 'white'}}>Langkah {this.state.currentPosition+1}</Text>
            </View>
          </View>
          <View style={{marginTop: 15, marginBottom: 15}}>
            <View style={{flexDirection: 'row', marginTop: 20}}>
                <Text style={{justifyContent: 'center', textAlign: 'center', fontFamily: 'Segoe UI', fontSize: 14, fontWeight: '500', color: this.state.colorLabelPhone, marginLeft: WINDOW.width*0.135}}>Hubungi{"\n"}Petugas</Text>
                <Text style={{justifyContent: 'center', textAlign: 'center', fontFamily: 'Segoe UI', fontSize: 14, fontWeight: '500', color: this.state.colorLabelCamera, marginLeft: WINDOW.width*0.140}}>Foto{"\n"}Kejadian</Text>
                <Text style={{justifyContent: 'center', textAlign: 'center', fontFamily: 'Segoe UI', fontSize: 14, fontWeight: '500', color: this.state.colorLabelReport, marginLeft: WINDOW.width*0.145}}>Kirim{"\n"}Laporan</Text>
            </View>
          </View>
          <View style={{marginHorizontal: 20}}>
                <StepIndicator
                        customStyles={customStyles}
                        currentPosition={this.state.currentPosition}
                        stepCount={3}
                />
          </View>
          <View style={{flexDirection: 'row', marginTop: 15}}>
                <View style={{marginLeft: WINDOW.width*0.18}}><VIcon type={VIcon.TYPE_FONTAWESOME} name="phone" size={25} color={this.state.colorPhone} /></View>
                <View style={{marginLeft: WINDOW.width*0.23}}><VIcon type={VIcon.TYPE_ENTYPO} name="camera" size={25} color={this.state.colorCamera} /></View>
                <View style={{marginLeft: WINDOW.width*0.23}}><VIcon type={VIcon.TYPE_ENTYPO} name="news" size={25} color={this.state.colorReport} /></View>
          </View>
          {
            this.state.currentPosition == 0 ?
            <View>
              <View style={{alignItems:'center', justifyContent: 'center', marginTop: WINDOW.height*0.1}}>
                <Text style={{fontFamily: 'Segoe UI', fontSize: 12}}>Anda akan diarahkan untuk menghubungi petugas</Text>
                <Text style={{fontFamily: 'Segoe UI', fontSize: 12}}>pemadam kebakaran sesuai dengan domisili anda</Text>
              </View>
              <TouchableOpacity onPress={()=>this.makePhonecall('081213683364')} style={{ width: WINDOW.width*0.5, height: WINDOW.height*0.07, marginTop: WINDOW.height*0.1, borderRadius: 25, backgroundColor: '#FF3737', alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                <Text style={{ fontSize: 15, color: "#FFFFFF", fontWeight: 'bold' }}>Telpon Sekarang</Text>
              </TouchableOpacity>
            </View> :
            this.state.currentPosition == 1 ?
            <View>
              <View style={{alignItems:'center', justifyContent: 'center', marginTop: WINDOW.height*0.1}}>
                <Text style={{fontFamily: 'Segoe UI', fontSize: 12}}>Ambil foto peristiwa kebakaran</Text>
                <Text style={{fontFamily: 'Segoe UI', fontSize: 12}}>yang sedang berlangsung</Text>
              </View>
              <TouchableOpacity onPress={()=>this.openCamera()} style={{ width: WINDOW.width*0.5, height: WINDOW.height*0.07, marginTop: WINDOW.height*0.1, borderRadius: 25, backgroundColor: '#FF3737', alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                <Text style={{ fontSize: 15, color: "#FFFFFF", fontWeight: 'bold' }}>Ambil Foto</Text>
              </TouchableOpacity>
            </View> :
            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 15}}>
              <ElevatedView 
                elevation={3} style={{
                  borderRadius: 15,
                  width: WINDOW.width *0.7, height: WINDOW.height*0.2,
                  backgroundColor: 'white',
                  // justifyContent: 'center',
                  // alignItems: 'center',
              }}>
                <Image style={{ height: WINDOW.height * 0.2, width: WINDOW.width * 0.7, borderRadius: 15 }} source={{ uri: this.state.img_uri }} />
              </ElevatedView>
              <View style={{alignItems:'center', justifyContent: 'center', marginTop: 10}}>
                <Text style={{fontFamily: 'Segoe UI', fontSize: 12}}>Pastikan kembali data</Text>
                <Text style={{fontFamily: 'Segoe UI', fontSize: 12}}>yang akan anda laporkan</Text>
              </View>
              <TouchableOpacity onPress={()=>this.gotoFinish()} style={{ width: WINDOW.width*0.5, height: WINDOW.height*0.07, marginTop: 25, borderRadius: 25, backgroundColor: '#FF3737', alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                <Text style={{ fontSize: 15, color: "#FFFFFF", fontWeight: 'bold' }}>Kirim Laporan</Text>
              </TouchableOpacity>
            </View>
          }
        </View>        
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#5459E6', '#FF3737']} style={{width: WINDOW.width, flex: 0.08}} />
        
      </View>
    );
  }
}
const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize:30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#5459E6',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#979afc',
    stepStrokeUnFinishedColor: '#979afc',
    separatorFinishedColor: '#D4D4D4',
    separatorUnFinishedColor: '#D4D4D4',
    stepIndicatorFinishedColor: '#979afc',
    stepIndicatorUnFinishedColor: '#979afc',
    stepIndicatorCurrentColor: 'white',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: 'black',
    stepIndicatorLabelFinishedColor: 'white',
    stepIndicatorLabelUnFinishedColor: 'white',
    labelColor: 'black',
    labelSize: 13,
    currentStepLabelColor: 'black'
  }

function mapStateToProps(state, ownProps) {
  return {
    rootState: state.root
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LaporIntro);