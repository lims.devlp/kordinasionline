import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions, Image, TouchableOpacity} from "react-native";
import {Icon} from "native-base";

const screen = Dimensions.get('window');

const Styles = StyleSheet.create({
    wrapper : {
        width : screen.width * 90/100,
        height: screen.height * 0.15,
        borderRadius : 15,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default class lightboxConfirm extends Component{
    handleClose = () =>{
        this.props.navigator.dismissLightBox();
    }
    handleConfirm = () =>{
        console.log("handle confirm");
        this.props.data().test();
        this.props.navigator.dismissLightBox();
    }
    render(){
        const data = this.props.data();
        return (
            <View style={[{backgroundColor:"white"} , Styles.wrapper]}>
                <View style={{position:"absolute", top:10, right:15, zIndex:1000}}>
                </View>
                {/* <Image source={data.image} style={{width:"100%" ,  height: screen.width/2}} resizeMode="stretch"/> */}
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    {/* {data.content()} */}
                    <Text>{data.message}</Text>
                </View>
                <View style={{flexDirection: 'row', width: screen.width*0.4, justifyContent: 'space-around'}}>
                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={this.handleClose} >
                        <Text style={{color: 'black', marginTop: 15, fontWeight: 'bold'}}>TIDAK</Text>
                    </TouchableOpacity>
                    {
                        data.act == 'logout' ?
                        <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={this.handleConfirm} >
                            <Text style={{color: 'black', marginTop: 15, fontWeight: 'bold'}}>YA</Text>
                        </TouchableOpacity> :
                        <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={this.handleClose} >
                            <Text style={{color: 'black', marginTop: 15, fontWeight: 'bold'}}>YA</Text>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        )
    }
}