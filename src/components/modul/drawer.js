import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, AsyncStorage } from "react-native";
import { Navigation } from 'react-native-navigation';
import VIcon from '../../components/styles/VIcon';
import { connect } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import * as appActions from '../../redux/action';

const screen = Dimensions.get('window');

const Styles = StyleSheet.create({
    wrapper: {
        width: screen.width * 0.75,
        height: screen.height,
        overflow: 'hidden',
        //justifyContent: 'center',
        //alignItems: 'center',
    },
    linearGradient: {
        width: screen.width,
        height: screen.height * 0.3,
        paddingLeft: 15,
        paddingRight: 15,
        justifyContent: 'center',
    },
});

export class drawer extends Component {
    constructor(props) {
        super(props);
    }

    openDrawer = (screen) => {
        if (screen == 'profile') {
            this.props.navigator.toggleDrawer({
                side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
                animated: true, // does the toggle have transition animation or does it happen immediately (optional)
                to: 'closed' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
            });
            this.props.navigator.push({
                screen: 'damkar.drawer.profile',
                navigatorStyle: {
                    navBarHidden: true,
                },
            });
        } else if(screen == 'setting'){
            this.props.navigator.toggleDrawer({
                side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
                animated: true, // does the toggle have transition animation or does it happen immediately (optional)
                to: 'closed' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
            });
            this.props.navigator.push({
                screen: 'damkar.drawer.setting',
                navigatorStyle: {
                  navBarHidden: true,
                },
            });
        } else{
            this.props.navigator.toggleDrawer({
                side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
                animated: true, // does the toggle have transition animation or does it happen immediately (optional)
                to: 'closed' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
            });
            const data = {
                act: 'logout',
                message: 'Apakah anda yakin ingin keluar?',
                test: () => {this.goLogout()}
            }
            this.props.navigator.showLightBox({
                screen: 'damkar.lightbox.confirm',
                style: {
                    backgroundBlur: 'dark',
                    backgroundColor: '#44444480',
                    tapBackgroundToDismiss: false
                },
                passProps: {
                    data: () => (data)
                }
            })
        }

    }

    goLogout = async () =>{
        try{
            console.log("goLogout");
            await AsyncStorage.setItem('isLogin', '0');
            this.props.actions.appInitialized();
            Navigation.startSingleScreenApp({
                screen: {
                    screen: 'damkar.auth.login',
                    navigatorStyle: {
                        navBarHidden: true,
                    },
                },
                animationType: 'slide-down',
                portraitOnlyMode: true
            });
        } catch(err){
            console.log(err);
        }
    }

    render() {
        return (
            <View style={[{ backgroundColor: "white" }, Styles.wrapper]}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#5459E6', '#FF3737']} style={Styles.linearGradient}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ height: screen.height * 0.13, width: screen.height * 0.13, borderRadius: screen.height * 0.13 * 0.5, borderColor: 'white', borderWidth: 6, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: 'white', fontSize: screen.height * 0.06 }}>KR</Text>
                        </View>
                        <View style={{ justifyContent: 'center', marginLeft: 10 }}>
                            <Text style={{ color: 'white', fontSize: 18 }}>Kripton Haz</Text>
                            <Text style={{ color: 'white', fontSize: 10, marginTop: 5 }}>+6285881732869</Text>
                        </View>
                    </View>
                </LinearGradient>
                <TouchableOpacity onPress={() => this.openDrawer('profile')}>
                    <View style={{ flexDirection: 'row', left: screen.width * 0.04, marginTop: 15 }}>
                        <VIcon type={VIcon.TYPE_ENTYPO} name="user" size={18} color='#707070' />
                        <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                            <Text style={{ color: '#707070', fontSize: 16 }}>Profile akun</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.openDrawer('setting')}>
                    <View style={{ flexDirection: 'row', left: screen.width * 0.04, marginTop: 15 }}>
                        <VIcon type={VIcon.TYPE_IONICONS} name="md-settings" size={18} color='#707070' />
                        <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                            <Text style={{ color: '#707070', fontSize: 16 }}>Pengaturan</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.openDrawer('logout')}>
                    <View style={{ flexDirection: 'row', left: screen.width * 0.04, marginTop: 20 }}>
                        <VIcon type={VIcon.TYPE_IONICONS} name="md-log-out" size={18} color='#707070' />
                        <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                            <Text style={{ color: '#707070', fontSize: 16 }}>Keluar</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        rootState: state.root
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(appActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(drawer);