import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions, Image, TouchableOpacity} from "react-native";
import {Icon} from "native-base";

const screen = Dimensions.get('window');

const Styles = StyleSheet.create({
    wrapper : {
        width : screen.width * 90/100,
        height: screen.height * 0.15,
        borderRadius : 15,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default class lightboxAlert extends Component{

    handleClose = () =>{
        this.props.navigator.dismissLightBox();
    }

    hendleAction = () =>{
        this.props.data().test();
    }

    render(){
        const data = this.props.data();
        return (
            <View style={[{backgroundColor:"white"} , Styles.wrapper]}>
                <View style={{position:"absolute", top:10, right:15, zIndex:1000}}>
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text>{data.message}</Text>
                </View>
                {
                    data.act == true ? 
                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={this.hendleAction} >
                        <Text style={{color: 'red', marginTop: 15}}>DONE</Text>
                    </TouchableOpacity> : 
                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={this.handleClose} >
                        <Text style={{color: 'red', marginTop: 15}}>DONE</Text>
                    </TouchableOpacity>
                }
            </View>
        )
    }
}