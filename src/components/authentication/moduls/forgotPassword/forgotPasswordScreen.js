import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image, Dimensions } from 'react-native';
import { Row, Container, Content, Grid, Button, Form, Item, Icon, Input } from "native-base";
import { connect } from 'react-redux';
import * as appActions from '../../../../redux/action';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import { WINDOW } from '../../../constants';
import VIcon from '../../../styles/VIcon';
import LinearGradient from 'react-native-linear-gradient';

type Props = {};
export class Signup extends Component<Props> {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
  }

  gotoNext = () => {
    this.props.navigator.push({
      screen: 'damkar.auth.signupPassword',
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }

  goBack = () => {
    this.props.navigator.pop();
  }

  forgotPassword = () =>{
    this.props.navigator.showSnackbar({
        text: 'Data telah berhasil dikirim ke email.',
        textColor: 'black', // optional
        backgroundColor: '#e5c059', // optional
        duration: 'short' // default is `short`. Available options: short, long, indefinite
    });
  }

  render() {
    let error = false;
    if (this.props.form) {
      if (this.props.form.syncErrors) {
        error = true;
      } else {
        error = false;
      }
    } else {
      error = false;
    }
    return (
      <View style={{justifyContent: 'space-between', height: WINDOW.height}}>
        <View style={{flex: 0.92}}>
          <View style={{height: WINDOW.height*0.3, backgroundColor: '#5459E6'}}>
            <TouchableOpacity onPress={this.goBack} style={{marginLeft: 10, marginTop: 10}}>
                <VIcon type={VIcon.TYPE_FEATHER} name="arrow-left" size={25} color='white' />
            </TouchableOpacity>
            <View style={{ width: WINDOW.width, height: WINDOW.height*0.1, marginTop: 10, marginLeft: 10}}>
                <Text style={{fontFamily: 'Segoe UI', fontSize: WINDOW.height*0.07, color: 'white'}}>Lupa</Text>
                <Text style={{fontFamily: 'Segoe UI', fontSize: WINDOW.height*0.07, color: 'white'}}>Password?</Text>
            </View>
          </View>

          <View style={{ paddingLeft: 16, paddingRight: 16 }}>
            <Text style={{fontFamily: 'Segoe UI', fontSize: WINDOW.height*0.02, color: 'black', marginTop: WINDOW.height*0.05, textAlign: 'center'}}>Kami akan mengirimkan anda email notifikasi untuk{"\n"}konfirmasi perubahan password</Text>
            <Text style={{fontFamily: 'Segoe UI', fontSize: WINDOW.height*0.03, color: 'black', marginTop: WINDOW.height*0.1, textAlign: 'center', fontWeight: 'bold'}}>Masukan email anda</Text>
            <Item rounded style={{ marginTop: 10, marginBottom: 10 }}>
                <Input placeholder='example@gmail.com' />
            </Item>
            <Text style={{fontFamily: 'Segoe UI', fontSize: WINDOW.height*0.018, color: '#3F7DCE', textAlign: 'center', fontWeight: '500'}}>*Pastikan email yang anda masukan benar</Text>
            <TouchableOpacity onPress={()=>this.forgotPassword()} style={{ width: WINDOW.width*0.5, height: WINDOW.height*0.07, marginTop: WINDOW.height*0.07, borderRadius: 25, backgroundColor: '#FF3737', alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                <Text style={{ fontSize: 15, color: "#FFFFFF", fontWeight: 'bold' }}>Kirim</Text>
            </TouchableOpacity>
          </View>
        </View>        
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#5459E6', '#FF3737']} style={{width: WINDOW.width, flex: 0.08}} />
        
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    rootState: state.root
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);