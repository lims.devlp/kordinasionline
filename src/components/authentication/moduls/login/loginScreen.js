import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image, Dimensions, AsyncStorage } from 'react-native';
import { Row, Container, Content, Grid, Button, Form, Item, Icon, Input } from "native-base";
import { LoginManager, AccessToken, LoginButton } from "react-native-fbsdk";
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { connect } from 'react-redux';
import * as appActions from '../../../../redux/action';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import { WINDOW } from '../../../constants';
import VIcon from '../../../styles/VIcon';

type Props = {};
export class Login extends Component<Props> {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    
  }

  handleChangePage = (page) => {
    console.log("page = " + page);
    var screenName;
    if(page == 'SignUp'){
      screenName = 'damkar.auth.signup';
    } else if(page == 'ForgotPassword'){
      screenName = 'damkar.auth.forgotPassword';
    }
    this.props.navigator.push({
      screen: screenName,
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }

  gotoMenu = () => {
    this.storeToken();
    this.props.actions.login();
  }

  async storeToken() {
    try {
        await AsyncStorage.setItem('isLogin', '1');
    } catch (error) {
        console.log("something went wrong");
        console.log("error = "+error);
    }
}

  loginFacebook = () => {
    LoginManager.logInWithReadPermissions(["public_profile"]).then(
      (result) => {
        console.log("result = " + JSON.stringify(result));
        if (result.isCancelled) {
          console.log("dicancel");
        } else {
          console.log('Login success');
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              console.log('token = ' + data.accessToken.toString())
              const { navigator } = this.props;
              navigator.push({
                screen: 'damkar.home.map',
                navigatorStyle: {
                  navBarHidden: true,
                },
              });
            }
          )
        }
      },
      function (error) {
        console.log("error = " + error);
      }
    ).catch((err) => {
    });
  }

  loginGoogle = async () => {
    GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true }).then(() => {
      console.log("berhasil yee");
    }).catch((err) => {
      console.log("Play service error", err.code, err.message);
    })
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/gmail.readonly'],
      webClientId: '133739195286-i02sm98e78vpv4962j25gjm2nioqm6um.apps.googleusercontent.com',
    });

    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log("userInfo = " + JSON.stringify(userInfo));
      this.gotoMenu();
    } catch (error) {
      console.log("error = " + error.code);
    }
  }

  render() {
    let error = false;
    if (this.props.form) {
      if (this.props.form.syncErrors) {
        error = true;
      } else {
        error = false;
      }
    } else {
      error = false;
    }
    return (
      <View>
        <View style={{ width: WINDOW.width, height: 180, paddingVertical: 20, justifyContent: "center", alignItems: 'center' }}>
          <Image source={require('../../../../assets/logo_damkar.png')} style={{ width: 250, height: 180 }} resizeMode="contain" />
        </View>

        <View style={{ paddingLeft: 16, paddingRight: 16 }}>
          <Form>
            <Item rounded style={{ marginBottom: 5 }}>
              <Icon active type='EvilIcons' name='user' style={{ fontSize: 40, color: 'gray' }} />
              <Input placeholder='Email' />
            </Item>
            <Item rounded>
              <Icon active type='EvilIcons' name='lock' style={{ fontSize: 40, color: 'gray' }} />
              <Input placeholder='password' secureTextEntry={true} />
            </Item>
          </Form>

          <TouchableOpacity onPress={() => { this.handleChangePage("ForgotPassword") }} style={{ marginTop: 15, marginLeft: 15 }}>
            <Text style={{ fontSize: 13, fontWeight: "200", color: "#3F7DCE" }}>Lupa password?</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.gotoMenu()} style={{ width: 165, height: 40, marginTop: 25, borderRadius: 25, backgroundColor: error ? "#AAA" : "#EC008C", alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
            <Text style={{ fontSize: 15, color: "#FFFFFF", fontWeight: 'bold' }}>Masuk</Text>
          </TouchableOpacity>

          <View style={{ alignItems: "center", justifyContent: "center", marginTop: 20 }}>
            <Text style={{ fontSize: 15, color: '#B4B4B4' }}>Atau</Text>
          </View>

          <TouchableOpacity onPress={() => this.loginFacebook()} style={{ width: 200, height: 40, marginTop: 20, borderRadius: 2, borderWidth: 1, borderColor: '#B4B4B4', backgroundColor: "#3b5998", flexDirection: 'row', alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
            <View style={{ width: 50, height: 40, alignItems: 'center', justifyContent: 'center' }}>
              <Image source={require('../../../../assets/fb_icon.png')} style={{ width: 16, height: 16, position: 'absolute', left: 15 }} resizeMode="cover" />
            </View>
            <View style={{ width: 150, height: 40, justifyContent: 'center' }}>
              <Text style={{ fontSize: 12, color: "#FFFFFF" }}>Masuk dengan Facebook</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.loginGoogle()} style={{ width: 200, height: 40, marginTop: 10, borderRadius: 2, borderWidth: 1, borderColor: '#B4B4B4', backgroundColor: "#FFFFFF", flexDirection: 'row', alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
            <View style={{ width: 50, height: 40, alignItems: 'center', justifyContent: 'center' }}>
              <Image source={require('../../../../assets/google_icon.png')} style={{ width: 16, height: 16, position: 'absolute', left: 15 }} resizeMode="cover" />
            </View>
            <View style={{ width: 150, height: 40, justifyContent: 'center' }}>
              <Text style={{ fontSize: 12, color: "#000000" }}>Masuk dengan Google</Text>
            </View>
          </TouchableOpacity>

          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 35 }}>
            <Text style={{ marginLeft: 5, fontSize: 13, color: "#B4B4B4" }}>Belum memiliki akun?</Text>
            <TouchableOpacity onPress={() => { this.handleChangePage("SignUp") }}>
              <Text style={{ fontSize: 13, color: "#EC008C", fontWeight: 'bold', paddingLeft: 5 }}>Daftar Akun</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    rootState: state.root
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);