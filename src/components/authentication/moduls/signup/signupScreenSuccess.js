import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image, Dimensions,
    Animated, Easing, BackHandler} from 'react-native';
import { Row, Container, Content, Grid, Button, Form, Item, Icon, Input } from "native-base";
import { connect } from 'react-redux';
import * as appActions from '../../../../redux/action';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import { WINDOW } from '../../../constants';
import VIcon from '../../../styles/VIcon';
import LinearGradient from 'react-native-linear-gradient';
import LottieView from 'lottie-react-native';
import animSuccess from '../../../../assets/success.json';

type Props = {};
export class SignupSuccess extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
        lottieProgress: new Animated.Value(0),
        isPlaying: true
    }
    BackHandler.addEventListener('hardwareBackPress', this.backPress);
  }

  componentDidMount(){
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    }); 
  }

  componentWillUnmount(){
    BackHandler.removeEventListener("hardwareBackPress", this.backPress);
  }

  backPress = () =>{
    return true;
  }

  gotoLogin = () => {
    this.props.navigator.popToRoot();
  }

  onLottieLoad = () => {
    console.log('play lottie');
    Animated.timing(this.state.lottieProgress, {
        toValue: 1,
        duration: 4000
    }).start(({ finished }) => {
        if (finished) {
          console.log("selesai");
          // this.setState({ isPlaying: false});
          setTimeout(()=>{
            this.props.navigator.popToRoot();
          },1000);
        }
    });
  }

  render() {
    let error = false;
    if (this.props.form) {
      if (this.props.form.syncErrors) {
        error = true;
      } else {
        error = false;
      }
    } else {
      error = false;
    }
    return (
      <View style={{justifyContent: 'space-between', height: WINDOW.height}}>
        <View style={{flex: 0.92, justifyContent: 'center', alignItems: 'center'}}>
            <LottieView
                loop={false}
                onLayout={this.onLottieLoad}
                ref={animation => {
                    this.animation = animation;
                }}
                style={{
                    width: WINDOW.width*0.8,
                    height: WINDOW.width*0.8,
                }}
                source={animSuccess}
                progress={this.state.lottieProgress}
            />
            <Text style={{color: 'black', fontSize: 20, marginTop: -20}}>Pendaftaran akun berhasil!</Text>
            <Text>Akun anda siap digunakan</Text>
            {/* {
                this.state.isPlaying == false ? 
                <TouchableOpacity onPress={() => this.gotoLogin()} style={{ width: 165, height: 40, marginTop: 20, borderRadius: 25, backgroundColor: error ? "#AAA" : "#EC008C", alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                    <Text style={{ fontSize: 15, color: "#FFFFFF", fontWeight: 'bold' }}>Login</Text>
                </TouchableOpacity> : null
            } */}
        </View>        
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#5459E6', '#FF3737']} style={{width: WINDOW.width, flex: 0.08}} />
        
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    rootState: state.root
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupSuccess);