import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image, Dimensions } from 'react-native';
import { Row, Container, Content, Grid, Button, Form, Item, Icon, Input } from "native-base";
import { connect } from 'react-redux';
import * as appActions from '../../../../redux/action';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import { WINDOW } from '../../../constants';
import VIcon from '../../../styles/VIcon';
import LinearGradient from 'react-native-linear-gradient';

type Props = {};
export class SignupPassword extends Component<Props> {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
  }

  gotoNext = () => {
    this.props.navigator.push({
      screen: 'damkar.auth.signupSuccess',
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }

  goBack = () => {
    this.props.navigator.pop();
  }

  render() {
    let error = false;
    if (this.props.form) {
      if (this.props.form.syncErrors) {
        error = true;
      } else {
        error = false;
      }
    } else {
      error = false;
    }
    return (
      <View style={{justifyContent: 'space-between', height: WINDOW.height}}>
        <View style={{flex: 0.92}}>
          <TouchableOpacity onPress={this.goBack} style={{marginLeft: 10, marginTop: 10}}>
              <VIcon type={VIcon.TYPE_FEATHER} name="arrow-left" size={25} color='#5459E6' />
          </TouchableOpacity>
          <View style={{ width: WINDOW.width, height: WINDOW.height*0.1, marginTop: 20, marginBottom: 20, justifyContent: "center", alignItems: 'center', flexDirection: 'row' }}>
            <Text style={{fontSize: WINDOW.height*0.08, color: '#5459E6', fontFamily: 'Segoe UI', fontWeight: '500'}}>Daftar</Text>
            <Text style={{fontSize: WINDOW.height*0.08, color: '#FF3737', fontFamily: 'Segoe UI', fontWeight: '500'}}> Akun</Text>
          </View>

          <View style={{ paddingLeft: 16, paddingRight: 16 }}>
            <Form>
              <Text style={{color: 'black', fontSize: 16, fontWeight: 'bold', marginLeft: 3}}>Password</Text>
              <Item rounded style={{ marginTop: 5, marginBottom: 10 }}>
                <Input placeholder='Min. karakter 6 digit' />
              </Item>
              <Text style={{color: 'black', fontSize: 16, fontWeight: 'bold', marginLeft: 3}}>Konfirmasi Password</Text>
              <Item rounded style={{ marginTop: 5, marginBottom: 10 }}>
                <Input placeholder='Ulangi Password'/>
              </Item>
            </Form>

            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 35 }}>
                <TouchableOpacity onPress={() => this.gotoNext()} style={{ width: 165, height: 40, marginTop: 20, borderRadius: 25, backgroundColor: error ? "#AAA" : "#EC008C", alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                    <Text style={{ fontSize: 15, color: "#FFFFFF", fontWeight: 'bold' }}>Submit</Text>
                </TouchableOpacity>
            </View>
          </View>
        </View>        
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#5459E6', '#FF3737']} style={{width: WINDOW.width, flex: 0.08}} />
        
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    rootState: state.root
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupPassword);