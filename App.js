import React, { Component } from 'react';
import { View, AppRegistry, AsyncStorage, Dimensions } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { registerScreens } from './src/config/screens';
// import { iconsMapFa, iconsLoadedFa } from './src/config/app-icons-fa';
import {Provider} from "react-redux";
import {createStore, applyMiddleware, combineReducers} from "redux";
import thunk from "redux-thunk";
import * as reducers from './src/redux/reducers/index';
import * as appActions from './src/redux/action/index';
// import bgMessaging from './src/config/bgMessaging';
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);
registerScreens(store, Provider); // this is where you register all of your app's screens
const screen = Dimensions.get('window');

export default class App extends Component{
    constructor(props){
        super(props);
        this.state = {
            nik: null,
            sesuatu: '0',
            player_id: undefined,
        }
        store.subscribe(this.onStoreUpdate.bind(this));
        store.dispatch(appActions.appInitialized());
    }

    componentDidMount() {
        console.log("initialize = "+store.getState().root);
    }

    componentWillUnmount() {
        
    }

    onStoreUpdate(){
        let {root} = store.getState().root;
        if(this.currentRoot != root){
            this.currentRoot = root;
            console.log("root nya = "+root);
            AsyncStorage.getItem('isLogin')
			.then((value) => {
              console.log('isLogin = ', value);
              this.startApp(root, value);
		    });
        }
    }

    async startApp(root, value){
        if(value == null || value == '0'){
            Navigation.startSingleScreenApp({
                screen: {
                    screen: 'damkar.auth.login', // unique ID registered with Navigation.registerScreen
                    navigatorStyle: {
                        navBarHidden: true, // make the nav bar hidden
                    }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
                    navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
                },
                passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                animationType: 'slide-down', // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                portraitOnlyMode: true
            });
        } else if(value == 1){
            Navigation.startSingleScreenApp({
                screen: {
                    screen: 'damkar.home.map', // unique ID registered with Navigation.registerScreen
                    navigatorStyle: {
                        navBarHidden: true, // make the nav bar hidden
                    }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
                    navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
                },
                drawer: {
                    // optional, add this if you want a side menu drawer in your app
                    left: {
                      // optional, define if you want a drawer from the left
                      screen: 'damkar.drawer.main', // unique ID registered with Navigation.registerScreen
                      passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                      disableOpenGesture: true, // can the drawer be opened with a swipe instead of button (optional, Android only)
                      //fixedWidth: screen.width*0.8 // a fixed width you want your left drawer to have (optional)
                    },
                    style: {
                      // ( iOS only )
                      drawerShadow: true, // optional, add this if you want a side menu drawer shadow
                      contentOverlayColor: 'rgba(0,0,0,0.25)', // optional, add this if you want a overlay color when drawer is open
                      leftDrawerWidth: 50, // optional, add this if you want a define left drawer width (50=percent)
                      rightDrawerWidth: 50 // optional, add this if you want a define right drawer width (50=percent)
                    },
                    type: 'MMDrawer', // optional, iOS only, types: 'TheSideBar', 'MMDrawer' default: 'MMDrawer'
                    animationType: 'door', //optional, iOS only, for MMDrawer: 'door', 'parallax', 'slide', 'slide-and-scale'
                    // for TheSideBar: 'airbnb', 'facebook', 'luvocracy','wunder-list'
                    disableOpenGesture: false // optional, can the drawer, both right and left, be opened with a swipe instead of button
                },
                passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                animationType: 'slide-down', // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                portraitOnlyMode: true
            });
        } else{
            console.log("error value condition = "+value);
        }
    }
}

// AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);